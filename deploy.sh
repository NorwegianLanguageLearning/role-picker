#!/bin/bash

set -e

gcloud functions deploy user_roles --runtime python37 --trigger-http --allow-unauthenticated --env-vars-file secrets.yml
