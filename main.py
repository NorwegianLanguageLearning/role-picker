import os
from typing import List, Dict, Any, Tuple, Union, Optional
import requests
from flask import Request, jsonify
import re
import enum

API_ENDPOINT = "https://discordapp.com/api/v6"
CLIENT_ID = "446812874615029763"
CLIENT_SECRET = os.environ["CLIENT_SECRET"]
REDIRECT_URI = os.environ.get(
    "REDIRECT_URI", "https://norwegianlanguagelearning.no/page/roles/"
)
BOT_TOKEN = os.environ["BOT_TOKEN"]
GUILD_ID = "143458761665675264"
CORS_ORIGIN = os.environ.get("CORS_ORIGIN", "https://norwegianlanguagelearning.no")


class Category(str, enum.Enum):
    TAGGABLE = "Taggable"
    PRONOUN = "Pronoun"
    LANGUAGE_NATIONALITY = "Language/Nationality"
    MAALFORM = "Målform"
    PROFICIENCY = "Proficiency"


USER_ASSIGNABLE_ROLES = {
    "498979431508082709": {
        "description": "for those who want to do reading practice together",
        "category": Category.TAGGABLE,
    },
    "500342470811451392": {
        "description": "for those wishing to be notified of the weekly Sunday calls",
        "category": Category.TAGGABLE,
    },
    "502968780201394186": {
        "description": "for those wishing to be tagged by other users for hanging out in voice channels",
        "category": Category.TAGGABLE,
    },
    "601008339429883904": {
        "description": "for those wishing to be tagged by other users for hanging out in voice channels talking Norwegian",
        "category": Category.TAGGABLE,
    },
    "144519407677210624": {
        "description": "for those natives willing to help out answering Norway-/Norwegian-related questions",
        "category": Category.TAGGABLE,
    },
    "612721189453824011": {
        "description": "for those who want to play games together",
        "category": Category.TAGGABLE,
    },
    "612721534062166016": {
        "description": "for playing video games like it's 2010 again",
        "category": Category.TAGGABLE,
    },
    "691110890824531989": {
        "description": "for those who specifically want to play Jackbox",
        "category": Category.TAGGABLE,
    },
    "698156453235785740": {
        "description": "for those who specifically want to play Stellaris",
        "category": Category.TAGGABLE,
    },
    "465279153189552155": {"description": "", "category": Category.PRONOUN},
    "465278992002580481": {"description": "", "category": Category.PRONOUN},
    "465279088991404062": {"description": "", "category": Category.PRONOUN},
    "465279058767511552": {"description": "", "category": Category.PRONOUN},
    "465278921374564362": {"description": "", "category": Category.PRONOUN},
    "465279122151702548": {"description": "", "category": Category.PRONOUN},
    "465279026890801182": {"description": "", "category": Category.PRONOUN},
    "510647835210022912": {
        "description": "for native Portuguese speakers (mainly Portugal and Brazil)",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "510651132696199189": {
        "description": "for people from Belarus",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "522501818542981120": {
        "description": "for people from the Netherlands",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "548490589763534852": {
        "description": "for speakers of Turkish",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "570649300586070046": {
        "description": "for speakers of Cantonese",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "451901913802211330": {
        "description": "for people from Sweden",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "249968322421063680": {
        "description": "for people from Denmark",
        "category": Category.LANGUAGE_NATIONALITY,
    },
    "510651182398832670": {
        "description": "for those who write mainly in Bokmål",
        "category": Category.MAALFORM,
    },
    "510651222424813568": {
        "description": "for those who write mainly in Nynorsk",
        "category": Category.MAALFORM,
    },
    "522501021776216110": {
        "description": "for those new to Norwegian",
        "category": Category.PROFICIENCY,
    },
    "522501326442332170": {
        "description": "for those with some experience with Norwegian",
        "category": Category.PROFICIENCY,
    },
    "522501673298558979": {
        "description": "for those advanced Norwegian users (i.e. conversational and higher)",
        "category": Category.PROFICIENCY,
    },
    "522501731368828934": {
        "description": "for those fluent in Norwegian",
        "category": Category.PROFICIENCY,
    },
    "622243009530036234": {
        "description": "for native speakers of Norwegian",
        "category": Category.PROFICIENCY,
    },
}

session = requests.Session()


def user_roles(request: Request):
    if request.method == "OPTIONS":
        headers = {
            "Access-Control-Allow-Origin": CORS_ORIGIN,
            "Access-Control-Allow-Methods": ", ".join(["GET", "PUT"]),
        }
        if request.access_control_request_headers:
            headers[
                "Access-Control-Allow-Headers"
            ] = request.access_control_request_headers
        return (
            "",
            204,
            headers,
        )
    elif request.method == "GET":
        return get_user_roles(request)
    elif request.method == "PUT":
        return change_user_roles(request)
    else:
        return (
            "",
            405,
            {"Access-Control-Allow-Origin": CORS_ORIGIN, "Allow": "GET, PUT"},
        )


def get_user_roles(request: Request):
    headers = {"Access-Control-Allow-Origin": CORS_ORIGIN}
    code = request.args.get("code")
    if not code:
        return jsonify({"detail": "Missing code"}), 400, headers
    try:
        access_token = exchange_code(code)
    except requests.HTTPError as e:
        if e.response.status_code == 400:
            return jsonify({"detail": "Invalid code"}), 400, headers
        raise
    user_id = get_user_id(access_token)
    member = get_member(user_id)
    if member is None:
        return jsonify({"detail": "User not found"}), 404, headers
    existing_roles = member["roles"]
    if not is_approved(existing_roles):
        return jsonify({"detail": "Not approved"}), 403, headers
    user_roles = list(set(USER_ASSIGNABLE_ROLES).intersection(existing_roles))
    roles = get_roles()
    return (
        jsonify({"userRoles": user_roles, "roles": roles, "accessToken": access_token}),
        200,
        headers,
    )


def change_user_roles(request: Request):
    headers = {
        "Access-Control-Allow-Origin": CORS_ORIGIN,
    }
    access_token = get_access_token(request)
    if not access_token:
        return jsonify({"detail": "Missing access token"}), 400, headers
    roles = request.get_json()
    if roles is None:
        return jsonify({"detail": "Missing roles"}), 400, headers

    user_id = get_user_id(access_token)
    member = get_member(user_id)
    if member is None:
        return jsonify({"detail": "User not found"}), 404, headers
    existing_roles = member["roles"]
    if not is_approved(existing_roles):
        return jsonify({"detail": "Not approved"}), 403, headers
    existing_non_user_assignable_roles = set(existing_roles).difference(
        USER_ASSIGNABLE_ROLES
    )
    requested_user_assignable_roles = set(roles).intersection(USER_ASSIGNABLE_ROLES)
    new_roles = list(
        requested_user_assignable_roles.union(existing_non_user_assignable_roles)
    )
    response = session.patch(
        f"{API_ENDPOINT}/guilds/{GUILD_ID}/members/{user_id}",
        json={"roles": new_roles},
        headers={"Authorization": f"Bot {BOT_TOKEN}"},
    )
    response.raise_for_status()
    return "", 204, headers


def exchange_code(code) -> str:
    data = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": REDIRECT_URI,
        "scope": "identify",
    }
    r = session.post(f"{API_ENDPOINT}/oauth2/token", data=data)
    r.raise_for_status()
    return r.json()["access_token"]


def refresh_token(refresh_token):
    data = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "grant_type": "refresh_token",
        "refresh_token": refresh_token,
        "redirect_uri": REDIRECT_URI,
        "scope": "identify",
    }
    r = session.post(f"{API_ENDPOINT}/oauth2/token", data=data)
    r.raise_for_status()
    return r.json()["access_token"]


def get_roles() -> List[Dict[str, Any]]:
    response = session.get(
        f"{API_ENDPOINT}/guilds/{GUILD_ID}/roles",
        headers={"Authorization": f"Bot {BOT_TOKEN}"},
    )
    response.raise_for_status()
    return [
        {
            "id": role["id"],
            "name": role["name"],
            "color": role["color"],
            **USER_ASSIGNABLE_ROLES[role["id"]],
        }
        for role in response.json()
        if role["id"] in USER_ASSIGNABLE_ROLES
    ]


def get_user_id(access_token: str) -> str:
    response = session.get(
        f"{API_ENDPOINT}/users/@me",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    response.raise_for_status()
    return response.json()["id"]


def get_member(user_id: str) -> Optional[Dict[str, Any]]:
    response = session.get(
        f"{API_ENDPOINT}/guilds/{GUILD_ID}/members/{user_id}",
        headers={"Authorization": f"Bot {BOT_TOKEN}"},
    )
    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        if e.response.status_code == 404:
            return None
    return response.json()


def get_access_token(request: Request) -> Optional[str]:
    access_token = request.headers.get("Authorization")
    if access_token is None:
        return None
    match = re.fullmatch("Bearer (\w+)", access_token)
    if match is None:
        return None
    return match[1]


def is_approved(roles: List[str]) -> bool:
    return "476133390664335361" in roles


if __name__ == "__main__":
    from flask import Flask, request

    def user_roles_view():
        return user_roles(request)

    os.environ["FLASK_ENV"] = "development"
    app = Flask(__name__)
    app.route("/user_roles/", methods=["GET", "OPTIONS", "PUT"])(user_roles_view)
    app.run()
